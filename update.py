#!/usr/bin/env python3
import json
import os
import urllib.request
from pathlib import Path
from sync import (
    makedir,
    sync_json,
)


def quantify_version(version):
    return tuple(map(int, version.split(".")))


def read_json(source):
    if source.startswith("http"):
        return json.loads(urllib.request.urlopen(source).read())
    else:
        with open(source) as f:
            return json.load(f)


def dl_file(source, directory, filename=None, **kwargs):
    silent = kwargs.get("silent")
    makedir(directory, silent=True)
    destination = Path(directory, filename) if filename else Path(directory, os.path.basename(source))
    if not silent:
        print('Downloading "%s" to "%s" ...' % (source, destination))
    return urllib.request.urlretrieve(source, destination)


def grab_releases():
    # variables
    releaseApi = "https://api.github.com/repos/Pathoschild/SMAPI/releases"
    releaseDir = "releases"
    releaseName = "%s.zip"

    # get latest local release
    local_releases = sync_json()["releases"]
    latest_local = local_releases[0]["version"] if local_releases else None

    # get latest remote release
    remote_releases = read_json(releaseApi)
    latest_remote = remote_releases[0]["tag_name"] if remote_releases else None

    if not latest_remote:
        print("Failed to determine the latest release from the remote source")
        return

    if latest_local and latest_local == latest_remote:
        print("The local release is already up to date (%s)" % latest_local)
        return

    # download newer releases
    for release in reversed(remote_releases):
        version = release["tag_name"]
        if not latest_local or quantify_version(version) > quantify_version(latest_local):
            release_url = release["assets"][-1]["browser_download_url"]
            print('Grabbing release "%s" ...' % (version))
            dl_file(release_url, releaseDir, releaseName % (version))
            latest_local = version

    # sync json file
    return sync_json()


# ! ========= DO NOT MODIFY BELOW THIS LINE ========= !

if __name__ == "__main__":
    grab_releases()