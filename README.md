# SMAPI Releases

## Description

This repository archives all the releases of the [Stardew Valley Modding API (SMAPI)](https://smapi.io) from version 3.13.1 onwards for scripting purposes.

## Usage

Use the `SMAPI.json` file maintained in this repository to get the available releases and make up their download links.

### Example

Load and read the `SMAPI.json` file:

```py
url = "https://gitlab.com/irfanhakim/smapi-release/-/raw/master/SMAPI.json"
smapi_json = json.loads(urllib.request.urlopen(url).read())
```

Make up the release info containing their version numbers and corresponding download IDs:

```py
release_info = {release["version"]: release["id"] for release in smapi_json["releases"]}
```

Get the latest SMAPI version and its unique download ID:

```py
# get the latest smapi version
latest_tag = max(release_info.keys(), key=lambda v: tuple(map(int, v.split("."))))

# get the id corresponding to the latest version
latest_id = release_info[latest_tag]
```

Make up the download URL for the latest SMAPI release:

```py
latest_smapi_url = smapi_json["base_url"] % latest_id
```

Download the latest SMAPI release:

```py
urllib.request.urlretrieve(latest_smapi_url, "SMAPI.zip")
```
