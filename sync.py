#!/usr/bin/env python3
import json
import os
import re


def extract_version(release):
    pattern = r"(\d+\.\d+\.\d+)"
    match = re.search(pattern, release)
    return match.group(1) if match else None


def normalise_release(release):
    return release.replace(" ", "%20")


def makedir(directory, **kwargs):
    silent = kwargs.get("silent")
    if not silent:
        print('Creating directory "%s" ...' % (directory))
    os.makedirs(directory, exist_ok=True)
    return directory


def sync_json():
    # variables
    baseUrl = "https://gitlab.com/irfanhakim/smapi-release/-/raw/master/releases/%s"
    releaseDir = "releases"
    jsonFile = "SMAPI.json"

    # create default json dict
    data = {
        "base_url": baseUrl,
        "releases": [],
    }

    # create releases directory if not exists
    makedir(releaseDir, silent=True)
    # get all releases
    releases = os.listdir(releaseDir)
    # sort releases in descending order
    releases.sort(reverse=True)

    # create dictionary of releases
    for release in releases:
        version = extract_version(release)
        if version:
            data["releases"].append({
                "id": normalise_release(release),
                "version": version,
            })

    # write to json file
    with open(jsonFile, "w") as f:
        f.write(json.dumps(data, indent=4))

    return data


# ! ========= DO NOT MODIFY BELOW THIS LINE ========= !

if __name__ == "__main__":
    sync_json()